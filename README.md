# VoidLanguages

Languages for Void addons

What is this?
-------------------

You can create a merge request here by simply creating a new language/editing an existing translation.
If your merge request gets approved, it will be automatically sent to servers with the addon.
